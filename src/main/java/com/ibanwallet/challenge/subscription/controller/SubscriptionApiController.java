package com.ibanwallet.challenge.subscription.controller;

import com.ibanwallet.challenge.subscription.model.SubscriptionModel;
import com.ibanwallet.challenge.subscription.request.SubscriptionRequest;
import com.ibanwallet.challenge.subscription.service.SubscriptionService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/subscription")
@Api(value = "Subscription Services")
public class SubscriptionApiController {

    @Autowired
    private SubscriptionService subscriptionService;

    @PostMapping(produces = "application/json")
    @ApiOperation("Create subscriptions for campaign service")
    public SubscriptionModel create(@RequestBody SubscriptionRequest subscription) {
        return subscriptionService.create(subscription);
    }

}
