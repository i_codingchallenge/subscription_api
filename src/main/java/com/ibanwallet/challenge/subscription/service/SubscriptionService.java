package com.ibanwallet.challenge.subscription.service;

import com.ibanwallet.challenge.subscription.model.SubscriptionModel;
import com.ibanwallet.challenge.subscription.request.SubscriptionRequest;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class SubscriptionService {

    @Value("${subscription.base.url}")
    private String apiSubscriptionBaseUrl;

    public SubscriptionModel create(SubscriptionRequest subscription) {
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<SubscriptionModel> response = restTemplate.postForEntity(new StringBuilder().append(apiSubscriptionBaseUrl).append("/subscription").toString(), subscription, SubscriptionModel.class);

        System.out.println(response.getBody());

        return response.getBody();
    }

}
